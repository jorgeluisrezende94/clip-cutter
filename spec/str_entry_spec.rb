# frozen_string_literal: true

RSpec.describe Clip::SRTEntry do
  describe "#shift_time" do
    subject(:entry) { Clip::SRTEntry.new }
    let(:intro_time) { "00:00:00,000" }
    before do
      entry.parse_time("01:12:00,010 --> 02:12:05,430")
      entry.shift_time_to(intro_time)
    end
    it { expect(entry.start_time).to eql("00:00:00,000") }
    it { expect(entry.end_time).to eql("01:00:05,420") }
  end
end

