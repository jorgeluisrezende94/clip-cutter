1
00:00:00,000 --> 00:00:05,430
olá pessoal fábio akita já que eu tenho

2
00:00:03,060 --> 00:00:08,610
falado sobre isso tudo acho que vale a

3
00:00:05,430 --> 00:00:11,309
pena entrar no tema do temeroso inglês

4
00:00:08,610 --> 00:00:12,870
todo programador sabe que deveria já

5
00:00:11,309 --> 00:00:16,080
sabem inglês mas fica empurrando

6
00:00:12,870 --> 00:00:18,690
empurrando até o dia que começa a perder

7
00:00:16,080 --> 00:00:20,100
oportunidades e aí percebe como já é

8
00:00:18,690 --> 00:00:22,890
tarde demais

9
00:00:20,100 --> 00:00:25,769
vamos dizer o óbvio para com as

10
00:00:22,890 --> 00:00:27,840
justificativas qualquer razão que você

11
00:00:25,769 --> 00:00:32,130
está usando para não evoluir no inglês

12
00:00:27,840 --> 00:00:35,309
não é nada mais nada menos que puro bush

13
00:00:32,130 --> 00:00:38,030
a foca em big five tarde a esse

14
00:00:35,309 --> 00:00:41,219
imperialismo temos que ser patriotas

15
00:00:38,030 --> 00:00:44,850
valorizar a língua nacional do a blau

16
00:00:41,219 --> 00:00:47,190
mimi focka se você diz imbecilidades

17
00:00:44,850 --> 00:00:49,590
como essa tão entendido então vão

18
00:00:47,190 --> 00:00:55,619
avançar

19
00:00:49,590 --> 00:00:55,619
[Música]

20
00:00:55,969 --> 00:01:02,180
tudo que sai de mais importante na área

21
00:00:59,399 --> 00:01:04,830
da tecnologia sai primeiro em inglês

22
00:01:02,180 --> 00:01:07,260
sabe o que acontece se você depende de

23
00:01:04,830 --> 00:01:09,960
material em português você precisa

24
00:01:07,260 --> 00:01:12,420
esperar essa tecnologia ganhar público

25
00:01:09,960 --> 00:01:14,610
alguém se a sca traduzir material em

26
00:01:12,420 --> 00:01:16,850
português e publicar aqui e todo mundo

27
00:01:14,610 --> 00:01:19,290
sabe que todo o projeto open source

28
00:01:16,850 --> 00:01:22,470
principalmente as primeiras interações

29
00:01:19,290 --> 00:01:25,170
muda o tempo todo ou seja do ponto de

30
00:01:22,470 --> 00:01:27,930
vista de retorno financeiro não é um bom

31
00:01:25,170 --> 00:01:31,020
negócio publicar livros de versões 1.0

32
00:01:27,930 --> 00:01:33,210
de projetos open source porque o 2.0 vai

33
00:01:31,020 --> 00:01:35,939
sair em seis meses e o livro vai ter

34
00:01:33,210 --> 00:01:38,759
pouco tempo de vida livro técnico é

35
00:01:35,939 --> 00:01:41,430
absolutamente chato de escrever eu sei

36
00:01:38,759 --> 00:01:43,110
eu já escrevi um especialmente porque se

37
00:01:41,430 --> 00:01:44,939
você demora muito tem que ficar

38
00:01:43,110 --> 00:01:47,820
revisando todos os capítulos anteriores

39
00:01:44,939 --> 00:01:49,439
quando saem um update e depois que

40
00:01:47,820 --> 00:01:52,470
publica ainda tem que ficar correndo

41
00:01:49,439 --> 00:01:54,600
atrás se sai outros upgrades que quebram

42
00:01:52,470 --> 00:01:57,600
os exemplos que você colocou no livro ou

43
00:01:54,600 --> 00:01:59,490
seja é um [ __ ] tranco de manter o nível

44
00:01:57,600 --> 00:02:01,920
no atualizado ao mesmo tempo que o

45
00:01:59,490 --> 00:02:05,070
software ainda está inacabado evoluindo

46
00:02:01,920 --> 00:02:07,500
mundo open source é assim mesmo e se por

47
00:02:05,070 --> 00:02:10,590
acaso um novo projeto consegue ganhar o

48
00:02:07,500 --> 00:02:10,950
seu público de orly adopters e começa a

49
00:02:10,590 --> 00:02:13,860
ter

50
00:02:10,950 --> 00:02:16,530
manda sabe quem mais ganha dinheiro quem

51
00:02:13,860 --> 00:02:19,200
aprendeu sobre o software antes mesmo de

52
00:02:16,530 --> 00:02:22,470
ter saído da versão 1.0 depois que saiu

53
00:02:19,200 --> 00:02:24,989
a quatro cinco e sete que já tem livros

54
00:02:22,470 --> 00:02:28,620
já têm curso já tem tonelada diz klink

55
00:02:24,989 --> 00:02:31,050
cast é quando a taxa agora vale menos 25

56
00:02:28,620 --> 00:02:33,720
10 vezes menos do que era pago para quem

57
00:02:31,050 --> 00:02:34,470
se arriscou e aprendeu na versão 10.1

58
00:02:33,720 --> 00:02:36,780
alfa

59
00:02:34,470 --> 00:02:38,970
você já entender uma coisa meio óbvia as

60
00:02:36,780 --> 00:02:41,549
pessoas ficam procurando o que a maioria

61
00:02:38,970 --> 00:02:43,920
das pessoas está fazendo eu penso assim

62
00:02:41,549 --> 00:02:46,319
[ __ ] se a maioria das pessoas está

63
00:02:43,920 --> 00:02:48,870
fazendo uma coisa porque essa coisa vale

64
00:02:46,319 --> 00:02:51,299
muito pouco e nunca vão me pagar muito

65
00:02:48,870 --> 00:02:53,640
justamente porque tem um milhão de

66
00:02:51,299 --> 00:02:56,760
indianos que vão cobrar dez vezes menos

67
00:02:53,640 --> 00:02:59,400
que eu pra fazer a mesma coisa e claro o

68
00:02:56,760 --> 00:03:01,620
mais seguro é primeiro e atrás de alguma

69
00:02:59,400 --> 00:03:04,170
coisa bem óbvia por isso eu falei no

70
00:03:01,620 --> 00:03:07,140
episódio de o que devo estudar pra ir no

71
00:03:04,170 --> 00:03:08,459
java se sharp o php da vida mas com o

72
00:03:07,140 --> 00:03:11,069
próximo passo

73
00:03:08,459 --> 00:03:14,130
ora quem ganha mais na indústria as

74
00:03:11,069 --> 00:03:16,950
moscas brancas dos cisnes negros mas é

75
00:03:14,130 --> 00:03:18,959
muito raro você escolher conscientemente

76
00:03:16,950 --> 00:03:22,019
a tecnologia que ainda vai se

77
00:03:18,959 --> 00:03:24,209
popularizar e acertar em cheio precisa

78
00:03:22,019 --> 00:03:26,579
de muita experiência para isso e mesmo

79
00:03:24,209 --> 00:03:29,010
assim você pode estar errado se eu fosse

80
00:03:26,579 --> 00:03:32,609
resumir você precisa saber duas coisas

81
00:03:29,010 --> 00:03:34,709
importantes história e línguas mas não

82
00:03:32,609 --> 00:03:37,739
do jeito que você pensa todo mundo fica

83
00:03:34,709 --> 00:03:40,319
atrás só dos rebanhos da notícia de hoje

84
00:03:37,739 --> 00:03:43,319
eu estou mais interessado em juntar as

85
00:03:40,319 --> 00:03:45,540
peças das notícias de ontem de um ano

86
00:03:43,319 --> 00:03:48,420
atrás de uma década atrás

87
00:03:45,540 --> 00:03:50,760
eu fico constantemente comparando estas

88
00:03:48,420 --> 00:03:53,010
coisas não tem uma receita mas quanto

89
00:03:50,760 --> 00:03:55,290
mais você estuda a história da sua área

90
00:03:53,010 --> 00:03:58,109
mas você vai entendendo para onde as

91
00:03:55,290 --> 00:04:00,959
coisas têm mais probabilidade de isso se

92
00:03:58,109 --> 00:04:02,549
chama reconhecimento de paternidade

93
00:04:00,959 --> 00:04:06,090
eu vou voltar nisso no fim do vídeo

94
00:04:02,549 --> 00:04:08,940
agora finalmente línguas que é o tema de

95
00:04:06,090 --> 00:04:11,639
hoje existem dezenas de formas de fazer

96
00:04:08,940 --> 00:04:13,669
isso de novo eu não vou dizer o óbvio um

97
00:04:11,639 --> 00:04:16,530
óbvio está em todos os canais do youtube

98
00:04:13,669 --> 00:04:16,959
de línguas em uma rápida procurada no

99
00:04:16,530 --> 00:04:19,169
google

100
00:04:16,959 --> 00:04:23,199
vai trazer uma tonelada de cursos online

101
00:04:19,169 --> 00:04:25,240
baixa ao domingo e jazz foquem do at

102
00:04:23,199 --> 00:04:27,849
mas o que eu quero fazer é explicar como

103
00:04:25,240 --> 00:04:29,949
eu encaro o aprendizado de qualquer

104
00:04:27,849 --> 00:04:32,500
coisa usando o meu aprendizado de inglês

105
00:04:29,949 --> 00:04:35,500
como exemplo sabe que tipo de pessoa

106
00:04:32,500 --> 00:04:37,630
aprende coisas mais rápido uma criança

107
00:04:35,500 --> 00:04:40,090
todo mundo já ouviu falar que toda

108
00:04:37,630 --> 00:04:41,830
criança é uma esponja que aprendi tudo

109
00:04:40,090 --> 00:04:43,870
que você ensina pra elas

110
00:04:41,830 --> 00:04:46,090
eu sempre ouvi falar isso e como toda a

111
00:04:43,870 --> 00:04:48,729
captação de regra que eu ouço eu parei

112
00:04:46,090 --> 00:04:51,190
para refletir pesquisar sobre isso eu me

113
00:04:48,729 --> 00:04:54,039
considero uma criança em muitos aspectos

114
00:04:51,190 --> 00:04:56,650
até hoje uma delas é que eu não tenho

115
00:04:54,039 --> 00:04:58,990
muito apego a nada portanto eu tenho

116
00:04:56,650 --> 00:05:01,780
muito pouco a perder e isso é uma

117
00:04:58,990 --> 00:05:03,910
criança não tem nada pra perder o melhor

118
00:05:01,780 --> 00:05:06,070
ela nunca parou pra considerar que tem

119
00:05:03,910 --> 00:05:08,409
coisas pra perder e por isso não tem

120
00:05:06,070 --> 00:05:10,900
medo de arriscar nada tudo pra ela

121
00:05:08,409 --> 00:05:13,900
oportunidade para que eu venho repetindo

122
00:05:10,900 --> 00:05:16,240
muito isso nos episódios anteriores no

123
00:05:13,900 --> 00:05:18,580
caso de línguas todo adulto começa

124
00:05:16,240 --> 00:05:20,590
pensando que tem muito a perder

125
00:05:18,580 --> 00:05:23,320
o adulto começa a pensar no que os

126
00:05:20,590 --> 00:05:24,130
outros vão pensar como os outros vão

127
00:05:23,320 --> 00:05:25,630
julgar

128
00:05:24,130 --> 00:05:28,180
será que vão dizer que estou demorando

129
00:05:25,630 --> 00:05:29,139
muito para aprender e achar que eu sou

130
00:05:28,180 --> 00:05:31,270
devagar

131
00:05:29,139 --> 00:05:34,150
será que vão dizer que eu não consigo

132
00:05:31,270 --> 00:05:36,310
falar e acha que eu sou incapaz será que

133
00:05:34,150 --> 00:05:38,050
não ficar tirando o sarro de mim e eu

134
00:05:36,310 --> 00:05:40,719
vou virar piada do grupo

135
00:05:38,050 --> 00:05:43,870
daí você pára e pensa se eu vou aprender

136
00:05:40,719 --> 00:05:46,659
tem que ser sério se é pra ser sério

137
00:05:43,870 --> 00:05:48,610
precisa ser um curso em concurso top só

138
00:05:46,659 --> 00:05:51,009
que curso top é caro e eu não tenho

139
00:05:48,610 --> 00:05:52,960
dinheiro agora e silva pagar caro

140
00:05:51,009 --> 00:05:56,289
precisa reservar muito tempo senão não

141
00:05:52,960 --> 00:05:58,719
compensa e um como eu tenho nem dinheiro

142
00:05:56,289 --> 00:06:00,909
e nem tempo vamos deixar o próximo

143
00:05:58,719 --> 00:06:03,099
semestre que aí vai dar tempo de guardar

144
00:06:00,909 --> 00:06:05,800
dinheiro organizar minha vida pra te

145
00:06:03,099 --> 00:06:09,340
tempo daí passa seis meses e o que

146
00:06:05,800 --> 00:06:11,500
acontece nada agora tenta se lembrar de

147
00:06:09,340 --> 00:06:13,900
quando você era criança ou qualquer

148
00:06:11,500 --> 00:06:16,750
criança que você conheça sobrinho filho

149
00:06:13,900 --> 00:06:19,750
neto aos dois anos de idade quantos

150
00:06:16,750 --> 00:06:21,690
cursos top de português você fez quantas

151
00:06:19,750 --> 00:06:24,639
horas em salas de aula com professores

152
00:06:21,690 --> 00:06:26,920
especializados a criança fez quantas

153
00:06:24,639 --> 00:06:29,800
certificações de cursos

154
00:06:26,920 --> 00:06:32,680
ela tirou não é possível que você nunca

155
00:06:29,800 --> 00:06:35,710
tenha pensado que em dois anos uma

156
00:06:32,680 --> 00:06:38,170
criança sai do zero pra razoavelmente

157
00:06:35,710 --> 00:06:40,570
influente numa língua o suficiente para

158
00:06:38,170 --> 00:06:43,300
você entender sentenças inteiras e em

159
00:06:40,570 --> 00:06:45,640
mais dois anos até quase tão influente

160
00:06:43,300 --> 00:06:48,220
quanto você ou melhor se bobear e

161
00:06:45,640 --> 00:06:50,500
obviamente não adianta falar com ela de

162
00:06:48,220 --> 00:06:53,740
coisas técnicas específicas falar em

163
00:06:50,500 --> 00:06:55,840
bolsa de valores física engenharia tudo

164
00:06:53,740 --> 00:06:58,090
vocabulários carro não vai saber mais

165
00:06:55,840 --> 00:06:59,770
larga essa criança num shopping center e

166
00:06:58,090 --> 00:07:02,170
certamente ela vai conseguir se

167
00:06:59,770 --> 00:07:04,960
comunicar com todo mundo e se ela não

168
00:07:02,170 --> 00:07:07,570
entender alguma coisa ela vai perguntar

169
00:07:04,960 --> 00:07:09,970
perguntar perguntar até ficar satisfeita

170
00:07:07,570 --> 00:07:13,360
com a resposta que é exatamente o que

171
00:07:09,970 --> 00:07:15,640
você não faz não subestime esse processo

172
00:07:13,360 --> 00:07:17,110
todo mundo que é esse processo na

173
00:07:15,640 --> 00:07:19,600
verdade por isso você vai fazer

174
00:07:17,110 --> 00:07:22,060
intercâmbio todo mundo tem essa intuição

175
00:07:19,600 --> 00:07:24,780
de que se você fizer uma imersão num

176
00:07:22,060 --> 00:07:27,250
país estrangeiro você vai magicamente

177
00:07:24,780 --> 00:07:29,500
aprender mais rápido qualquer

178
00:07:27,250 --> 00:07:32,590
metodologia que te incentive a se

179
00:07:29,500 --> 00:07:34,750
concentrar acima do normal num período

180
00:07:32,590 --> 00:07:38,020
limitado de tempo funciona da mesma

181
00:07:34,750 --> 00:07:40,390
forma indo para o exterior ou não e por

182
00:07:38,020 --> 00:07:42,700
exterior pra mim é mais status do que

183
00:07:40,390 --> 00:07:45,040
necessidade mais claro se puder com

184
00:07:42,700 --> 00:07:47,620
certeza vá a experiência maior é a

185
00:07:45,040 --> 00:07:50,200
vivência mais do que o estudo trabalha

186
00:07:47,620 --> 00:07:52,570
num bar para o seu curso passa um ano

187
00:07:50,200 --> 00:07:54,340
morando fora você provavelmente não vai

188
00:07:52,570 --> 00:07:56,590
precisar meus vídeos se tivesse feito

189
00:07:54,340 --> 00:07:59,380
isso e claro você precisa ter um mínimo

190
00:07:56,590 --> 00:08:01,780
de estrutura nem que seja as porcarias

191
00:07:59,380 --> 00:08:04,750
de aulas de inglês que todos nós tivemos

192
00:08:01,780 --> 00:08:07,240
a nossa escola que ensinou sol basicão

193
00:08:04,750 --> 00:08:09,760
do verbo to be e meia dúzia de palavras

194
00:08:07,240 --> 00:08:12,040
mas só deve ser o suficiente eu

195
00:08:09,760 --> 00:08:13,720
mencionei num dos vídeos que quando eu

196
00:08:12,040 --> 00:08:16,330
comecei a fazer faculdade

197
00:08:13,720 --> 00:08:18,550
meu inglês não era fluente eu ia devagar

198
00:08:16,330 --> 00:08:20,800
e não tinha influência nenhuma para

199
00:08:18,550 --> 00:08:23,140
falar nem pra arriscar frases

200
00:08:20,800 --> 00:08:24,910
o que me motivou foi o primeiro dia de

201
00:08:23,140 --> 00:08:27,400
aula o professor está livros

202
00:08:24,910 --> 00:08:29,080
estrangeiros na bibliografia na lousa eu

203
00:08:27,400 --> 00:08:31,840
tenho colegas de classe que já tinha

204
00:08:29,080 --> 00:08:34,270
morado fora ou feito intercâmbio estavam

205
00:08:31,840 --> 00:08:36,010
na minha frente e eu já disse também que

206
00:08:34,270 --> 00:08:37,250
é o tipo de coisa que me deixa motivado

207
00:08:36,010 --> 00:08:39,560
a correa

208
00:08:37,250 --> 00:08:42,140
mas e foi exatamente o que eu fiz e

209
00:08:39,560 --> 00:08:46,220
aprendizado é que nem dieta ou você faz

210
00:08:42,140 --> 00:08:49,190
ou ruim sem compromissos ou não faz do

211
00:08:46,220 --> 00:08:52,280
odor do nordeste não foque em troy

212
00:08:49,190 --> 00:08:54,440
vamos lá assim como 99% população eu só

213
00:08:52,280 --> 00:08:56,630
comprava livros e revistas em português

214
00:08:54,440 --> 00:08:58,910
eu só assistia a filme dublado com

215
00:08:56,630 --> 00:09:01,430
legenda ouvir a música mas sequer

216
00:08:58,910 --> 00:09:03,020
prestar atenção nas letras e evitava

217
00:09:01,430 --> 00:09:05,090
tudo o que tinha em inglês

218
00:09:03,020 --> 00:09:07,460
eu fiz aqueles cursinhos de bairro tipo

219
00:09:05,090 --> 00:09:10,070
fisk mas eu desisti porque não tinha

220
00:09:07,460 --> 00:09:12,140
evolução era essa a minha situação no

221
00:09:10,070 --> 00:09:15,140
ano que eu entrei na faculdade eu nunca

222
00:09:12,140 --> 00:09:16,910
fiquei 100% perfeito na fluência mas ou

223
00:09:15,140 --> 00:09:21,500
são vocês mesmos

224
00:09:16,910 --> 00:09:26,090
ok só a spad next hanners under lockton

225
00:09:21,500 --> 00:09:30,080
e mais mãe de saída de josé ueno ordenar

226
00:09:26,090 --> 00:09:33,380
inglês cours mitchell demonstrou a nevo

227
00:09:30,080 --> 00:09:36,140
querida classe c na grande desnível

228
00:09:33,380 --> 00:09:38,960
versão online kimi mas também são

229
00:09:36,140 --> 00:09:41,930
novamente deu antes de detê lo onde fez

230
00:09:38,960 --> 00:09:45,830
muito difícil julifest de never land

231
00:09:41,930 --> 00:09:46,550
rover um óleo spiess cost maneira

232
00:09:45,830 --> 00:09:49,640
américa

233
00:09:46,550 --> 00:09:55,490
essa hipótese público bilhão até 2011

234
00:09:49,640 --> 00:09:58,780
líder deles ex em tens every day s favor

235
00:09:55,490 --> 00:10:02,330
levem nail a 6 edição ar e foi movida

236
00:09:58,780 --> 00:10:05,510
ernani uramb any corte diz ruiz orsi

237
00:10:02,330 --> 00:10:08,360
senil erson a equipe forte browsing de

238
00:10:05,510 --> 00:10:12,170
web inglês gol de borges e nobu em

239
00:10:08,360 --> 00:10:15,770
bailes onde books e magazines white goat

240
00:10:12,170 --> 00:10:19,430
de shine theater em leite what the boxer

241
00:10:15,770 --> 00:10:24,200
primers ou línguas sol e boston ziemer

242
00:10:19,430 --> 00:10:27,500
amy erin esecs em vai me em maiô um thin

243
00:10:24,200 --> 00:10:30,410
high never bota ou era single buoy mega

244
00:10:27,500 --> 00:10:33,740
sena em português é braguim à espera

245
00:10:30,410 --> 00:10:35,120
grande coxa ou mais móvel e vai impor em

246
00:10:33,740 --> 00:10:38,510
campo e books

247
00:10:35,120 --> 00:10:42,170
a moça tem fai taymor vai mega sim site

248
00:10:38,510 --> 00:10:45,050
wired works e mega sim ranking de nuvens

249
00:10:42,170 --> 00:10:46,070
é boa pois ter na rede tipo a box em

250
00:10:45,050 --> 00:10:49,400
falta dele

251
00:10:46,070 --> 00:10:52,490
em 27 ray de português a firewalls ele

252
00:10:49,400 --> 00:10:55,910
fosse nature andré rhayner what's the

253
00:10:52,490 --> 00:10:58,790
fire nuves meditar dissolver bill read

254
00:10:55,910 --> 00:11:02,990
my blogs e no facebook e twitter

255
00:10:58,790 --> 00:11:06,590
imagens tv jay mohr dem twiters do herói

256
00:11:02,990 --> 00:11:09,890
que blairo e nem 2s mancha saicã de se

257
00:11:06,590 --> 00:11:12,650
perceber que em maio tinha tudo desde a

258
00:11:09,890 --> 00:11:14,900
avó de cans filme brasil em esforce dias

259
00:11:12,650 --> 00:11:17,090
tabela ainda um clube brasileiro em

260
00:11:14,900 --> 00:11:20,750
books ainda um ótimo brasileiro teve

261
00:11:17,090 --> 00:11:23,900
shows e descanse dor normal a 10 de not

262
00:11:20,750 --> 00:11:25,130
what the open teve caso netflix para a

263
00:11:23,900 --> 00:11:28,990
vinda

264
00:11:25,130 --> 00:11:32,780
sem dominar slides e a 0 ou discorde

265
00:11:28,990 --> 00:11:35,270
lista deck e um tio netflix é mais um

266
00:11:32,780 --> 00:11:38,150
kindle just made in my life exigir

267
00:11:35,270 --> 00:11:40,580
quando a helsinque rolled a idade o

268
00:11:38,150 --> 00:11:44,900
maior problema em inglês

269
00:11:40,580 --> 00:11:48,530
o excel bom time sport diz verbo names

270
00:11:44,900 --> 00:11:49,310
em inglês bee statement centex justin

271
00:11:48,530 --> 00:11:52,790
long

272
00:11:49,310 --> 00:11:55,670
donde wii e for nothing else onde eu ia

273
00:11:52,790 --> 00:11:58,220
lá está difícil e os portugues e monta

274
00:11:55,670 --> 00:12:00,590
ever been bolt open source

275
00:11:58,220 --> 00:12:03,800
a nossa ideia de um chute ou pessoas é

276
00:12:00,590 --> 00:12:07,130
verify boy aí com 11 em the doors ou o

277
00:12:03,800 --> 00:12:10,760
som é eo que raia programa de

278
00:12:07,130 --> 00:12:13,790
intercâmbio e audir vai em couro em

279
00:12:10,760 --> 00:12:17,750
português limites de hit all that could

280
00:12:13,790 --> 00:12:20,660
à espera um fire sexy years câncer estão

281
00:12:17,750 --> 00:12:23,510
lhe deu inter 6 no ideb foi mais força

282
00:12:20,660 --> 00:12:26,570
no iêmen khaled de dot com where many

283
00:12:23,510 --> 00:12:29,750
verdade que é sem dó kalman amor que

284
00:12:26,570 --> 00:12:31,790
forçou de um ano emite fighter florida

285
00:12:29,750 --> 00:12:34,130
office guerra com ênio vecchi óleo

286
00:12:31,790 --> 00:12:37,550
diesel terç desde o hospital usando um

287
00:12:34,130 --> 00:12:41,930
ano em tons de verde foi está em 7

288
00:12:37,550 --> 00:12:44,990
furini weslley mention de um ano o time

289
00:12:41,930 --> 00:12:51,100
bridge onde a partir de rogers deve

290
00:12:44,990 --> 00:12:56,720
expressar isso lima e põe um mês de chãs

291
00:12:51,100 --> 00:12:59,360
mtel e não amam alf will save em prédios

292
00:12:56,720 --> 00:13:02,660
eu ia lá em cima de mt x polônia vai

293
00:12:59,360 --> 00:13:05,660
vender diz as quais de frank williams

294
00:13:02,660 --> 00:13:09,019
anders peniche max english as she wolf

295
00:13:05,660 --> 00:13:10,310
the hot one de tempo na equipe eo funk

296
00:13:09,019 --> 00:13:13,579
sex in the office

297
00:13:10,310 --> 00:13:16,360
ouço e cleise defecou ex-ante fol a

298
00:13:13,579 --> 00:13:21,079
ithink a quem deus dengue berti de

299
00:13:16,360 --> 00:13:26,269
formas de reforçar tenta incutir pires

300
00:13:21,079 --> 00:13:30,470
lowe meditamos em dom de ouvir the years

301
00:13:26,269 --> 00:13:32,930
ahmed que puder bank time é de se

302
00:13:30,470 --> 00:13:36,560
perguntar por que o filho chen guang

303
00:13:32,930 --> 00:13:39,379
sánchez icollector nispel em sé eo lusa

304
00:13:36,560 --> 00:13:43,730
na partida ele o aniversário collectania

305
00:13:39,379 --> 00:13:47,680
em gays ei ei itens som e se desde que o

306
00:13:43,730 --> 00:13:51,220
leco leilão online força de seu elenco o

307
00:13:47,680 --> 00:13:55,850
filme no twitter e nem deve sair é fino

308
00:13:51,220 --> 00:14:00,800
literal do próprio joel sem fluir

309
00:13:55,850 --> 00:14:03,829
warwick de dous de del bosque neft num

310
00:14:00,800 --> 00:14:08,360
cofre ou frango mais um dinar straits

311
00:14:03,829 --> 00:14:12,680
times children e servem a bordo do audi

312
00:14:08,360 --> 00:14:15,949
está o irmão sauína chai o nome existem

313
00:14:12,680 --> 00:14:27,110
chile larry eo adauto céu é brinde kills

314
00:14:15,949 --> 00:14:30,529
shellback shin beth ernest hemingway

315
00:14:27,110 --> 00:14:33,889
questions lethem desde ontem o haiti

316
00:14:30,529 --> 00:14:37,819
landsoft o 4chan everything número real

317
00:14:33,889 --> 00:14:53,839
seja edição de ontem da ufems notw s

318
00:14:37,819 --> 00:14:56,870
quest means war with you ever desde que

319
00:14:53,839 --> 00:15:00,500
é de enurese empenho não tem cavalo e

320
00:14:56,870 --> 00:15:07,390
américas don't care de quental a nora

321
00:15:00,500 --> 00:15:10,900
ney e anderson bueno e na bm f

322
00:15:07,390 --> 00:15:15,040
gangues dentes gás pix berlim inglês

323
00:15:10,900 --> 00:15:18,220
nick ewers pikielny orlandi e net ray

324
00:15:15,040 --> 00:15:20,200
monstro américas com spin luz de um

325
00:15:18,220 --> 00:15:24,090
espectro é nenhum ano é que o espírito

326
00:15:20,200 --> 00:15:28,000
moda online onde andei aqui cheguei eo e

327
00:15:24,090 --> 00:15:31,420
morfin em angers tena botina américas

328
00:15:28,000 --> 00:15:33,760
hardwares definir ex de rawat no

329
00:15:31,420 --> 00:15:38,200
hospital frança méxico república

330
00:15:33,760 --> 00:15:44,190
dominicana times de rabi é a linha sem

331
00:15:38,200 --> 00:15:49,180
sobis ou for dar 12 alex round quando

332
00:15:44,190 --> 00:15:55,300
enfim wellington great britain de neuer

333
00:15:49,180 --> 00:16:00,460
o céu e sol de maiô texas dançantes ao

334
00:15:55,300 --> 00:16:05,980
new orleans barry white nadal

335
00:16:00,460 --> 00:16:09,130
aí eu achei for rocky ramon push the

336
00:16:05,980 --> 00:16:13,720
only one jornal historinha do inps

337
00:16:09,130 --> 00:16:17,650
raynal e senhor lazy netjets lazy ademir

338
00:16:13,720 --> 00:16:20,950
e terry e filme online store vai

339
00:16:17,650 --> 00:16:25,510
naufragar zillow box scheidt europa e

340
00:16:20,950 --> 00:16:27,070
insistem de fogo em agosto em 2009

341
00:16:25,510 --> 00:16:29,130
incluindo os cinco portugueses de

342
00:16:27,070 --> 00:16:33,600
cozinha quente fine fim à angústia

343
00:16:29,130 --> 00:16:36,670
inglês é the world of wings em língua

344
00:16:33,600 --> 00:16:39,100
portuguesa bull com mark webb sites

345
00:16:36,670 --> 00:16:42,130
scheidt e deixou língua de aluguel é

346
00:16:39,100 --> 00:16:49,810
browsers inglês na de scott kelly

347
00:16:42,130 --> 00:16:53,050
manning em estol 10 ground york nem 2010

348
00:16:49,810 --> 00:16:58,630
manchas e o que é ver e nem duas nem

349
00:16:53,050 --> 00:17:01,570
senti a mota-engil que os quais 11 eo

350
00:16:58,630 --> 00:17:09,540
channel sai reduziu o português tiago

351
00:17:01,570 --> 00:17:17,130
aos netflix stormers sampaio em 2001 e

352
00:17:09,540 --> 00:17:19,290
2010 o google eo off

353
00:17:17,130 --> 00:17:22,920
e tente de casa de um filme um carro

354
00:17:19,290 --> 00:17:26,730
fica boa duddy a fazer em action quente

355
00:17:22,920 --> 00:17:30,210
rosa de vídeo next new networks queres

356
00:17:26,730 --> 00:17:33,600
martin em google e de fumo e réptil de

357
00:17:30,210 --> 00:17:35,730
emiti-lo em processos ou mate e erh ray

358
00:17:33,600 --> 00:17:37,830
dura mas céu foi um exemplo

359
00:17:35,730 --> 00:17:40,980
a dias loureiro à aneel ou drive em

360
00:17:37,830 --> 00:17:43,590
beverly hills da flor ao recuar e último

361
00:17:40,980 --> 00:17:46,830
ensaio no retorno do iqa feira em natal

362
00:17:43,590 --> 00:17:50,610
e se porque ele é nosso orgulho e elas

363
00:17:46,830 --> 00:17:54,210
café society move ou nuvens o steamworks

364
00:17:50,610 --> 00:17:59,460
a energ 11/11/10 clemer ou sua equipe

365
00:17:54,210 --> 00:18:03,480
mãe de wisnik tecm ano em 17 12 noites

366
00:17:59,460 --> 00:18:06,690
action não leve isso it be' de português

367
00:18:03,480 --> 00:18:09,030
eu acho que eu sou diferente quando eu

368
00:18:06,690 --> 00:18:11,220
falo em inglês e português porque você

369
00:18:09,030 --> 00:18:13,170
não pode falar em inglês com o mesmo

370
00:18:11,220 --> 00:18:15,180
ritmo que fala português

371
00:18:13,170 --> 00:18:17,700
do mesmo jeito que seria bizarro você

372
00:18:15,180 --> 00:18:20,460
cantar por exemplo um fly me to the moon

373
00:18:17,700 --> 00:18:21,840
de frank sinatra com o ritmo de funk

374
00:18:20,460 --> 00:18:25,080
pensa como ia ficar

375
00:18:21,840 --> 00:18:28,110
você precisa respirar diferente e é mais

376
00:18:25,080 --> 00:18:30,210
fácil ver isso quando assistir séries e

377
00:18:28,110 --> 00:18:32,220
filmes começa a prestar atenção na

378
00:18:30,210 --> 00:18:34,830
linguagem corporal dos atores eu já

379
00:18:32,220 --> 00:18:37,920
fiquei horas fazendo careta pra te ver

380
00:18:34,830 --> 00:18:40,380
pra imitar a posição da boca e um jeito

381
00:18:37,920 --> 00:18:42,240
de inspirar e expirar as palavras e

382
00:18:40,380 --> 00:18:44,880
sobre respiração uma coisa que eu vejo

383
00:18:42,240 --> 00:18:47,010
90% das pessoas fazendo de errado fala

384
00:18:44,880 --> 00:18:48,990
as palavras até o fim não tentar

385
00:18:47,010 --> 00:18:51,360
conectar uma palavra na outra como

386
00:18:48,990 --> 00:18:54,000
fazemos falando português corrido fala

387
00:18:51,360 --> 00:18:57,300
inglês como um gaúcho ou sulista fala

388
00:18:54,000 --> 00:18:59,820
português pausado correto palavras até o

389
00:18:57,300 --> 00:19:02,130
fim com o pausa só isso já é meio

390
00:18:59,820 --> 00:19:03,720
caminho andado para ser entendido mas se

391
00:19:02,130 --> 00:19:05,670
você não está convencido eu vou dizer

392
00:19:03,720 --> 00:19:07,530
uma última coisa que eu acho muito

393
00:19:05,670 --> 00:19:10,350
importante inclusive tem a ver com

394
00:19:07,530 --> 00:19:12,510
programação no mundo da programação você

395
00:19:10,350 --> 00:19:15,810
freqüentemente vê o povo falando de

396
00:19:12,510 --> 00:19:16,800
padrões de programação ou padrões de

397
00:19:15,810 --> 00:19:19,290
arquitetura

398
00:19:16,800 --> 00:19:21,960
quando alguém fala padrão parece dizer

399
00:19:19,290 --> 00:19:24,990
no sentido de é a melhor forma de fazer

400
00:19:21,960 --> 00:19:27,770
alguma coisa isso tá errado em inglês

401
00:19:24,990 --> 00:19:32,000
temo três palavras diferentes

402
00:19:27,770 --> 00:19:34,280
de falta e para os 3 traduzem português

403
00:19:32,000 --> 00:19:36,170
padrão mas eles querem dizer três coisas

404
00:19:34,280 --> 00:19:38,960
completamente diferentes

405
00:19:36,170 --> 00:19:41,140
o que você pensa por padrão normalmente

406
00:19:38,960 --> 00:19:44,870
é o que o americano pensa quando

407
00:19:41,140 --> 00:19:48,230
distender uns entender a ser seguido ou

408
00:19:44,870 --> 00:19:50,120
você tem de funk a escolha padrão ou

409
00:19:48,230 --> 00:19:53,660
quando você não escolhe o que é

410
00:19:50,120 --> 00:19:55,820
escolhido é o de fofo finalmente pedra é

411
00:19:53,660 --> 00:19:58,940
simplesmente alguma coisa que aparece

412
00:19:55,820 --> 00:20:01,040
várias vezes pode ser certo ou errado um

413
00:19:58,940 --> 00:20:03,110
perde pode simplesmente indicar uma

414
00:20:01,040 --> 00:20:05,420
tendência por exemplo se todo mundo

415
00:20:03,110 --> 00:20:07,790
resolver começar a usar calça vermelha

416
00:20:05,420 --> 00:20:10,580
isso é uma moda eu posso dizer que é um

417
00:20:07,790 --> 00:20:13,460
padre ou dizer que toda boa pizza

418
00:20:10,580 --> 00:20:16,880
costuma ter queijo é um pedra mas não é

419
00:20:13,460 --> 00:20:18,620
um padrão ou 100% das pitas dele

420
00:20:16,880 --> 00:20:20,960
deveriam ter queijos entender a

421
00:20:18,620 --> 00:20:23,300
diferença por isso toda vez que em

422
00:20:20,960 --> 00:20:25,730
programação você vê algum livro ou

423
00:20:23,300 --> 00:20:28,940
matéria falando em padrão de arquitetura

424
00:20:25,730 --> 00:20:32,570
não pense que esse deve ser um jeito

425
00:20:28,940 --> 00:20:35,150
certo de fazer é só um jeito que naquele

426
00:20:32,570 --> 00:20:38,120
momento que o texto foi escrito o autor

427
00:20:35,150 --> 00:20:41,030
viu com alguma frequência o suficiente

428
00:20:38,120 --> 00:20:43,160
para chamar de perry em um ano pode ser

429
00:20:41,030 --> 00:20:46,040
já que está obsoleto porque assim como

430
00:20:43,160 --> 00:20:49,580
tendências pedras também ficam velhos

431
00:20:46,040 --> 00:20:51,740
outros pedras aparecem se você parar pra

432
00:20:49,580 --> 00:20:52,940
entender o básico sobre como o cérebro

433
00:20:51,740 --> 00:20:55,280
funciona

434
00:20:52,940 --> 00:20:57,380
do ponto de vista neurológico um de

435
00:20:55,280 --> 00:21:00,410
nossos melhores mecanismos é a nossa

436
00:20:57,380 --> 00:21:02,750
habilidade de aprender e detectar pedras

437
00:21:00,410 --> 00:21:04,970
é parte do que chamamos de instinto

438
00:21:02,750 --> 00:21:07,520
quando sentimos que vamos ser atacados e

439
00:21:04,970 --> 00:21:11,080
fugimos sem pensar o mesmo quando vemos

440
00:21:07,520 --> 00:21:11,080
textos como este

441
00:21:11,110 --> 00:21:18,319
[Música]

442
00:21:19,160 --> 00:21:25,290
nós não lemos palavras de letra em

443
00:21:22,590 --> 00:21:27,960
letras nós identificamos opera e vamos

444
00:21:25,290 --> 00:21:29,940
lendo em clusters às vezes pulamos uma

445
00:21:27,960 --> 00:21:32,670
sentença inteira se foi um clichê um

446
00:21:29,940 --> 00:21:34,830
chavão porque opera economiza nosso

447
00:21:32,670 --> 00:21:36,930
processamento é meio como uma rede

448
00:21:34,830 --> 00:21:39,330
neural artificial funciona ou o que

449
00:21:36,930 --> 00:21:42,450
chamamos hoje de machine learning é

450
00:21:39,330 --> 00:21:45,450
basicamente identificação de férias

451
00:21:42,450 --> 00:21:47,730
feres também têm efeitos colaterais é de

452
00:21:45,450 --> 00:21:49,860
onde temos as ilusões de ótica por

453
00:21:47,730 --> 00:21:54,200
exemplo onde vemos pernas que não

454
00:21:49,860 --> 00:21:54,200
existem ou peres que nos confundem

455
00:22:04,070 --> 00:22:09,210
psicologicamente é de onde vem muito de

456
00:22:06,810 --> 00:22:11,640
nossas neuroses e até mesmo teorias da

457
00:22:09,210 --> 00:22:14,250
conspiração superstições que são

458
00:22:11,640 --> 00:22:15,120
basicamente vermos pedras que não

459
00:22:14,250 --> 00:22:17,490
existem

460
00:22:15,120 --> 00:22:20,430
adicionar causalidade há coisas que não

461
00:22:17,490 --> 00:22:22,770
tem correlação tudo é parte dessa

462
00:22:20,430 --> 00:22:25,080
identificação de pedras quando

463
00:22:22,770 --> 00:22:27,270
aprendemos em inglês nos inserindo no

464
00:22:25,080 --> 00:22:29,490
ambiente como eu fiz artificialmente

465
00:22:27,270 --> 00:22:31,770
mesmo que eu não entenda tudo que eu

466
00:22:29,490 --> 00:22:34,500
estou lendo ouvindo seu praticar

467
00:22:31,770 --> 00:22:37,890
deliberadamente a repetição eu

468
00:22:34,500 --> 00:22:40,440
eventualmente vou sentir quando algo sua

469
00:22:37,890 --> 00:22:42,930
correta ou não eu não lembro a regra

470
00:22:40,440 --> 00:22:47,040
gramatical quando leio uma frase eu sei

471
00:22:42,930 --> 00:22:50,130
que soa estranho ou não aí onde oea ou a

472
00:22:47,040 --> 00:22:52,410
nossa gana do e são literalmente a mesma

473
00:22:50,130 --> 00:22:54,870
coisa mas naturalmente eu digo um ou

474
00:22:52,410 --> 00:22:57,270
outro sem pensar muito eu argumentaria

475
00:22:54,870 --> 00:22:58,950
que para o dia a dia isso é um bom nível

476
00:22:57,270 --> 00:23:01,800
quando você naturalmente fala uma frase

477
00:22:58,950 --> 00:23:04,320
de acordo com o contexto da conversa sem

478
00:23:01,800 --> 00:23:06,780
precisar conscientemente pensar na

479
00:23:04,320 --> 00:23:09,180
construção dessa frase que é exatamente

480
00:23:06,780 --> 00:23:11,850
como você fala na sua língua nativa não

481
00:23:09,180 --> 00:23:14,970
está a fazer um esforço consciente muito

482
00:23:11,850 --> 00:23:18,060
grande para simplesmente falar em resumo

483
00:23:14,970 --> 00:23:21,900
o que normalmente chamamos de padrão não

484
00:23:18,060 --> 00:23:23,320
é estender é para que você saberia disso

485
00:23:21,900 --> 00:23:25,300
se nesse os livros em

486
00:23:23,320 --> 00:23:28,270
leis em vez das traduções em português

487
00:23:25,300 --> 00:23:31,210
onde é impossível traduzir o contexto

488
00:23:28,270 --> 00:23:34,000
exatamente como o autor gostaria toda

489
00:23:31,210 --> 00:23:36,360
tradução perde parte do sentido não

490
00:23:34,000 --> 00:23:38,410
importa quão bom seja o tradutor

491
00:23:36,360 --> 00:23:41,230
especialmente porque algumas coisas têm

492
00:23:38,410 --> 00:23:43,390
carga filosófica ou cultural que não têm

493
00:23:41,230 --> 00:23:46,210
como traduzir um dos exemplos que eu

494
00:23:43,390 --> 00:23:49,570
mais gosto é quando traduzem making

495
00:23:46,210 --> 00:23:51,820
manny pra ganhar dinheiro tecnicamente

496
00:23:49,570 --> 00:23:54,880
está correto você diz a gol em que o

497
00:23:51,820 --> 00:23:57,400
work make manny que traduziria pra eu

498
00:23:54,880 --> 00:23:59,860
vou trabalhar para ganhar dinheiro em

499
00:23:57,400 --> 00:24:02,950
português a frase implica que sua opção

500
00:23:59,860 --> 00:24:06,130
é trocar seu trabalho por dinheiro em

501
00:24:02,950 --> 00:24:09,400
inglês não se diz ganhasse disse make é

502
00:24:06,130 --> 00:24:12,490
fazer a frase em inglês implica a opção

503
00:24:09,400 --> 00:24:14,380
de você produzir onde antes não existia

504
00:24:12,490 --> 00:24:17,080
alguma coisa que a essência de

505
00:24:14,380 --> 00:24:19,240
empreender e na minha cabeça é uma das

506
00:24:17,080 --> 00:24:21,280
maiores diferenças culturais dos estados

507
00:24:19,240 --> 00:24:21,970
unidos para qualquer outro lugar do

508
00:24:21,280 --> 00:24:25,030
mundo

509
00:24:21,970 --> 00:24:28,660
lá filosoficamente falando objetivo era

510
00:24:25,030 --> 00:24:31,600
construir e criar onde nada existia no

511
00:24:28,660 --> 00:24:34,510
resto do mundo é dividir e tomar o que

512
00:24:31,600 --> 00:24:37,210
existe ganhar o dinheiro ganhar as

513
00:24:34,510 --> 00:24:38,800
terras ganhar o poder não construir e

514
00:24:37,210 --> 00:24:41,110
com isso eu espero que tenha conseguido

515
00:24:38,800 --> 00:24:43,210
explicar o que eu acho sobre aprendizado

516
00:24:41,110 --> 00:24:45,010
e em particular o que eu acho que pode

517
00:24:43,210 --> 00:24:47,260
ajudar na sua jornada para aprender

518
00:24:45,010 --> 00:24:49,330
outra língua eu fiz mais ou menos a

519
00:24:47,260 --> 00:24:52,120
mesma coisa a aprender japonês

520
00:24:49,330 --> 00:24:54,580
em ambos os casos eu fiz se em curso até

521
00:24:52,120 --> 00:24:56,470
o básico e sair do curso sabe aqueles

522
00:24:54,580 --> 00:24:58,990
cursos de bairro que eu falei como

523
00:24:56,470 --> 00:25:01,090
autodidata eu acho que não foi tão ruim

524
00:24:58,990 --> 00:25:03,490
o que vocês acham e quais são as suas

525
00:25:01,090 --> 00:25:05,650
histórias não convencionais de como

526
00:25:03,490 --> 00:25:08,110
aprenderam línguas não deixe de comentar

527
00:25:05,650 --> 00:25:10,300
nos comentários abaixo se gostaram

528
00:25:08,110 --> 00:25:13,300
mantém um jóia compartilhe com seus

529
00:25:10,300 --> 00:25:17,190
amigos assim o canal e clique no sininho

530
00:25:13,300 --> 00:25:17,190
a gente se vê até mais

