# Clip Cutter

## THIS IS A PROOF-OF-CONCEPT

The idea:

* Given that I have transcripts for all my videos on YouTube
* Given that YouTube automatically timed the transcripts into an SRT file
* Given that I have the SRT files downloaded locally

* Then I can use OpenCV to locate my face in the videos
* Then I can pass a quote from the transcript and locate the start/end time
  in the SRT files
* Then I can cut based on the found times and crop based on the face detection
* Then I can embed the subtitles from the SRT files
* Then I can even speed up the final video to be more watchable on TikTok

## Installation

You need to install 'ffmpeg' and 'opencv'. In Arch:

    $ sudo pacman -S ffmpeg python python-pip opencv opencv-samples
    $ pip install opencv-python

    $ bundle install

This project still uses a Python script to integrate with OpenCV for
the face detection, so you need to have Python installed as well.

YouTube's automatic timing generation creates entries with overlapped
times, so there is a bin/subtitle-overlap-fixer binary and the source
code, in Go as well.

## Usage

This repository already provides an example of video and subtitle in the `spec/fixtures` directory. You can use it to improve the tests.

You can start by using the command line script:

    $ bin/clip_cutter -i spec/fixtures/Episode\ 0032.mp4 \
      -o /tmp/test.mp4
      -s spec/fixtures/Episode\ 0032.srt \
      -q "vamos dizer o óbvio para com as justificativas qualquer razão que você está usando para não evoluir no inglês"
      -text-style "text=DIZENDO<br/>O ÓBVIO"
      -a 0.8

To see all the available options:

    $ bin/clip_cutter --help

You can create an Excel file with the columns for:

1. directory of the videos, such as "/home/foo/videos"
2. directory of the subtitles, such as "/home/foo/documents/captions"
3. name of the video, such as "episode-1.mp4", and then it's assumed the subtitle will have the name "episode-1.srt"
4. the quote to search in the subtitle file

Then you can use this command line for batch processing:

    $ bin/clip_cutter -x spec/fixtures/quotes.xlsx -o /tmp -a 0.8

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bundle exec spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## TODO

* allow non-subsequent quotes, so we have to find the clips and concatenate them in the end (to be able to skip unnecessary parts in the middle)
* research YouTube Analytics API to see if it's possible to find the most watched parts, then align with the subtitle to trim the clip without cutting sentences in the middle (I'm still not sure this is worth it)


## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/akitaonrails/clip_cutter.

This project is licensed under the GNU General Public License v3.0. See the [LICENSE](LICENSE) file for details.
