# frozen_string_literal: true

require 'roo'
require 'open3'

module Batch
  class Excel
    def initialize(file_path, opt = {})
      @workbook = Roo::Spreadsheet.open(file_path)
      @opt = opt
      @output_dir = opt[:output]
    end

    def check_file(file, errors)
      return true if File.exist?(file)

      errors << { input_file: file, error: "error: #{file} not found" }
      return false
    end

    def process_quotes(dry_run = false, suppress_messages = false)
      exl = {}

      result = []
      errors = []
      @workbook.each do |row|
        exl[:video_dir]  = row[0] if row[0] && row[0] != exl[:video_dir]
        exl[:srt_dir]    = row[1] if row[1] && row[1] != exl[:srt_dir]
        exl[:video_name] = row[2] if row[2] && row[2] != exl[:video_name]
        exl[:srt_style]  = row[3] if row[3] && row[3] != exl[:srt_style]
        exl[:quote]      = row[4]
        exl[:status]     = row[5]
        # TODO: this column is not being updated in the spreadsheet

        next if exl[:status] == "processed" || !exl[:video_name]

        input_file = File.join(exl[:video_dir], exl[:video_name])
        next unless check_file input_file, errors

        no_ext_filename = exl[:video_name].gsub(".mov", "").gsub(".mp4", "")
        exl[:srt_file] = File.join(exl[:srt_dir], "#{no_ext_filename}.srt")
        next unless check_file exl[:srt_file], errors

        # many clips for the same video name file, so add timestamp to output file
        output_file = File.join(@output_dir, "#{no_ext_filename}-#{Time.now.strftime("%H-%M-%S")}.mp4")
        entry = {
          input_file: input_file,
          srt_file: exl[:srt_file],
          output_file: output_file,
          quote: exl[:quote]
        }

        if dry_run
          result << entry.merge(status: "dry_run")
        else
          cutter = Clip::Cutter.new(input_file, exl[:srt_file],
                                    speedup: @opt[:speedup] || 0.9,
                                    left_shift: @opt[:left_shift] || 15,
                                    srt_styles: exl[:srt_styles] || {},
                                    text_styles: @opt[:text_styles] || {})
          puts "Rendering clip: #{output_file}}" unless suppress_messages
          if cutter.cut_clip(exl[:quote], output_file)
            result << entry.merge(status: "processed")
          else
            result << entry.merge(status: "error")
          end
        end
      end
      [result, errors]
    end
  end
end
